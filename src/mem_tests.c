#include "mem.h"
#include "mem_internals.h"
#include "mem_tests.h"
#include "util.h"

typedef bool (*test)(void*, size_t, FILE*);

static struct block_header* find_block_before_last(void* heap){
	struct block_header* block = heap;
	if(!block)
		return NULL;
	struct block_header* previous_block = NULL;
	while(block->next){
		previous_block = block;
		block = block->next;
	}
	return previous_block;
}

static void clear_heap(void* heap){
	struct block_header* block = heap;
	while(block){
		_free(block->contents);
		block = block->next;
	}
	void* just_malloc = _malloc(1); // При выделении первый блок объединится
       	//со всеми остальными
	_free(just_malloc);
}

bool do_test(test test, FILE* log){
	if(!log)
		err("Bad log file");
	static void* heap;
	if(!heap)
		heap = heap_init(REGION_MIN_SIZE - offsetof(struct block_header, contents));
	if(!heap)
		err("Cannot allocate stack");
	bool result = test(heap, REGION_MIN_SIZE, log);
	clear_heap(heap);
	return result;
}

bool test_block(struct block_header* block){
	//Если нет SEGFAULT, то прошло
	for(size_t i = 0; i < block->capacity.bytes; ++i){
		block->contents[i] = 0;
	}
	return true;
}

static bool _test_region_border(void* heap, size_t heap_size, FILE* log){
	fprintf(log, "Initial heap\n");
	debug_heap(log, heap);
	 _malloc(heap_size - offsetof(struct block_header, contents));
	test_block(heap);
	fprintf(log, "All bytes in block are writable\n");
	return true;
}

static bool _test_mem_alloc(void*  heap, size_t heap_size, FILE* log){
	heap_size = 0;
	fprintf(log, "Initial heap\n");
	debug_heap(log, heap);
	size_t sz = 1 + heap_size;
	_malloc(sz);
	fprintf(log, "Heap after malloc(%zu)\n", sz);
	debug_heap(log, heap);
	struct block_header* block = heap;
	return block && !block->is_free && block->capacity.bytes >= sz;
}

static bool _test_free_one_block(void* heap, size_t heap_size, FILE* log){
	heap_size = 0;
	_malloc(20 + heap_size);
	void* test2 = _malloc(30);
	_malloc(40);
	fprintf(log, "%s\n", "Heap after three mallocs");
	debug_heap(log, heap);
	_free(test2);
	fprintf(log, "%s\n", "Freeing second block");
	debug_heap(log, heap);
	struct block_header* first_block = heap;
	bool result = first_block;
	if(!first_block)
		return false;
	struct block_header* second_block = first_block->next;
	result = result && second_block;
	if(!result)
		return false;
	struct block_header* third_block = second_block->next;
	return result && third_block && !first_block->is_free && second_block->is_free &&
		!third_block->is_free;
}

static bool _test_free_two_blocks(void* heap, size_t heap_size, FILE* log){
	heap_size = 0;
	_malloc(20 + heap_size);
	void* test2 = _malloc(30);
	void* test3 = _malloc(40);
	_malloc(50);
	fprintf(log, "%s\n", "Heap after four mallocs");
	debug_heap(log, heap);
	_free(test3);
	_free(test2);
	fprintf(log, "%s\n", "Heap after freeing two neighbouring blocks");
	debug_heap(log, heap);
	struct block_header* first_block = heap;
	if(!first_block && first_block->is_free)
		return false;
	struct block_header* second_block = first_block->next;
	if(!second_block && !second_block->is_free)
		return false;
	struct block_header* third_block = second_block->next;
	if(!third_block && third_block->is_free)
		return false;
	struct block_header* fourth_block = third_block->next;
	return (!fourth_block || (fourth_block && fourth_block->is_free)) && second_block->capacity.bytes == 30 + 40 + offsetof(struct block_header, contents);

}

static bool _test_extend_region_close(void* heap, size_t heap_size, FILE* log){
	void* test = _malloc(heap_size - offsetof(struct block_header, contents));
	_malloc(1024);
	fprintf(log, "%s\n", "Heap after overflow");
	debug_heap(log, heap);
	struct block_header* block = (void*) ((char*)test - offsetof(struct block_header, contents));
	void* next_block_should_start = (char*)test + block->capacity.bytes;
	return next_block_should_start == (char*)block->next && !block->next->is_free;
}

static bool _test_extend_region_somewhere(void* heap, size_t heap_size, FILE* log){
	_malloc(heap_size - offsetof(struct block_header, contents));
	struct block_header* block = find_block_before_last(heap);
	struct block_header* hidden_block = block->next;
	block->next = NULL; // Симулируем ситуацию, когда регион нельзя выделить вплотную
	_malloc(1024);
	fprintf(log, "%s\n", "Heap after overflow");
	debug_heap(log, heap);
	void* next_block_should_not_start = (char*)block + offsetof(struct block_header, contents) + block->capacity.bytes;
	bool result = block->next != next_block_should_not_start && !block->next->is_free;
	hidden_block->next = block->next;
	block->next = hidden_block;
	return result;
	
}

bool test_mem_alloc(FILE* log){
	return do_test(_test_mem_alloc, log);
}

bool test_region_border(FILE* log){
	return do_test(_test_region_border, log);
}

bool test_free_one_block(FILE* log){
	return do_test(_test_free_one_block, log);
}

bool test_free_two_blocks(FILE* log){
	return do_test(_test_free_two_blocks, log);
}

bool test_extend_region_close(FILE* log){
	return do_test(_test_extend_region_close, log);
}

bool test_extend_region_somewhere(FILE* log){
	return do_test(_test_extend_region_somewhere, log);
}

#include "mem.h"
#include "mem_tests.h"
#include <stddef.h>
#include <stdio.h>

void print_test_success(bool result){
	if(result){
		printf("\nTest passed\n");
	}
	else{
		printf("\nTest did not pass\n");
	}
}

int main(){
	puts("Test region border");
	print_test_success(test_region_border(stdout));	
	puts("\nTest simple malloc");
	print_test_success(test_mem_alloc(stdout));
	puts("\nTest freeing one block");
	print_test_success(test_free_one_block(stdout));
	puts("\nTest freeing two neigbourong blocks");
	print_test_success(test_free_two_blocks(stdout));
	puts("\nTest allocating close to initial region");
	print_test_success(test_extend_region_close(stdout));
	puts("\nTest allocating somewhere not near to initial region");
	print_test_success(test_extend_region_somewhere(stdout));
	return 0;
}
